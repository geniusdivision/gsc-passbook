<?php
/**
 * The development database settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=gscdemo',
			'username'   => 'root',
			'password'   => 'root',
		),
	),
);
