<h2>Listing Events</h2>
<br>
<?php if ($events): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Date</th>
			<th>Location</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($events as $event): ?>		<tr>

			<td><?php echo $event->name; ?></td>
			<td><?php echo $event->date; ?></td>
			<td><?php echo $event->location; ?></td>
			<td>
				<?php echo Html::anchor('events/view/'.$event->id, 'View'); ?> |
				<?php echo Html::anchor('events/edit/'.$event->id, 'Edit'); ?> |
				<?php echo Html::anchor('events/delete/'.$event->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Events.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('events/create', 'Add new Event', array('class' => 'btn btn-success')); ?>

</p>
