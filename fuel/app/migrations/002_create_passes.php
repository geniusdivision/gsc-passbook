<?php

namespace Fuel\Migrations;

class Create_passes
{
	public function up()
	{
		\DBUtil::create_table('passes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'auth_token' => array('constraint' => 255, 'type' => 'char'),
			'pass_type_id' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('passes');
	}
}