<?php

class Model_Pass extends \Model_PassBase
{
	protected static $_properties = array(
		'id',
		'auth_token',
		'pass_type_id',
		'created_at',
		'updated_at'
	);

	private $key_path = "gsc";
	private $source_path = "gsc/source";
	private $key_password = "12345";
	private $pass_type_id = "pass.com.geeksocialclub.event2";

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	function __construct()
	{
		parent::__construct($this->key_path);
		$this->content["passTypeIdentifier"] = $this->pass_type_id;
	}
}
