<?php

class Model_PassBase extends \Model_Crud
{
    private $work_folder = null;
	private $ID = null;
	
	var $content = null;
	var $pass_bundle_file = null;
	
	function __construct($folder_name)
	{
		assert(file_exists(APPPATH.DS."passes/".$folder_name."/pass.json"));
		
		$this->ID = uniqid();

		$this->work_folder = sys_get_temp_dir()."/".$this->ID;
		mkdir($this->work_folder);
		assert(file_exists($this->work_folder));

		$this->copy_to_work_folder(APPPATH.DS."passes/".$folder_name);
	
		$this->read_pass_from_json_file($this->work_folder."/pass.json");
	}
	
	function __destruct()
	{
		$this->cleanup();
	}

	private function copy_to_work_folder($path) {
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path),RecursiveIteratorIterator::SELF_FIRST);
		
		foreach ($files as $name => $file)
		{
			if (is_file($name) && substr($file->getFileName(), 0, 1) != ".")
			{
				copy($name, $this->work_folder."/".str_replace($path."/", "",$name));	
			}	
			elseif (is_dir($name))
			{
				mkdir($this->work_folder."/".str_replace($path."/", "",$name));	
			}
		}
	}
	
	function read_pass_from_json_file($path)
	{
		$this->content = json_decode(file_get_contents($path),true);
	}
	
	function write_pass_to_json_file() {
		file_put_contents($this->work_folder."/pass.json",json_encode($this->content));
	}
	
	function write_recursive_manifest()
	{
		$manifest = new ArrayObject();
		
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->work_folder),RecursiveIteratorIterator::SELF_FIRST);
		
		foreach ($files as $name => $file)
		{
			if (is_file($name) && substr($file->getFileName(), 0, 1) != ".")
			{
				$relative_name = str_replace($this->work_folder."/", "", $name);
				$sha1 = sha1(file_get_contents($file->getRealPath()));
				$manifest[$relative_name] = $sha1;
			}
		}

		file_put_contents($this->work_folder."/manifest.json",json_encode($manifest));
	}
	
	function generate_bundle_signature($key_path,$password)
	{
		$key_path = realpath($key_path);
		
		if (!file_exists($key_path."/certificates/wwdr.pem"))
		{
			die("No WWDR cert found");	
		}
		
		if (!file_exists($key_path."/certificates/certificate.pem"))
		{
			die("No pass cert found");	
		}
		
		if (!file_exists($key_path."/certificates/key.pem"))
		{
			die("No pass key found");	
		}
		
		$output = shell_exec("openssl smime -binary -sign".
		" -certfile '".$key_path."/certificates/wwdr.pem'".
		" -signer '".$key_path."/certificates/certificate.pem'".
		" -inkey '".$key_path."/certificates/key.pem'".
		" -in '".$this->work_folder."/manifest.json'".
		" -out '".$this->work_folder."/signature'".
		" -outform DER -passin pass:'".$password."'"
		);
	}
	
	function generate_pass_bundle()
	{
		$pass_file = $this->work_folder."/".$this->ID.".pkpass";
		
		$zip = new ZipArchive();
		$success = $zip->open($pass_file, ZIPARCHIVE::OVERWRITE);
		if ($success !== TRUE) die("Can't create zip file");
		
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->work_folder),RecursiveIteratorIterator::SELF_FIRST);
		
		foreach ($files as $name => $file)
		{
			if (is_file($name) && substr($file->getFileName(), 0, 1) != ".")
			{
				$relative_name = str_replace($this->work_folder."/", "", $name);
				
				$zip->addFile($file->getRealPath(),$relative_name);
			}
		}
		
		$zip->close();
		
		$this->pass_bundle_file = $pass_file;
		
		return $pass_file;
	}
	
	function cleanup()
	{
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->work_folder),RecursiveIteratorIterator::CHILD_FIRST);	
		
		foreach ($files as $name => $file)
		{
			if (is_file($name))
			{
				unlink($name);
			}	
			elseif (is_dir($name))
			{
				rmdir($name);
			}
		}
		
		rmdir($this->work_folder);
	}

	function download()
	{
		
		
	}
}