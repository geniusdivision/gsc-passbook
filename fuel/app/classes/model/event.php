<?php
use Orm\Model;

class Model_Event extends Model
{
	protected static $_properties = array(
		'id',
		'name',
		'date',
		'location',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[255]');
		$val->add_field('date', 'Date', 'required');
		$val->add_field('location', 'Location', 'required|max_length[255]');

		return $val;
	}

}
