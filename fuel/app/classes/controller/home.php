<?php

class Controller_Home extends Controller_Template
{

	public function action_index()
	{
		$this->template->title = 'Events';
		$this->template->content = View::forge('event/index');
	}

	public function action_get_pass()
	{
		$ticket = \Model_Pass::forge();
		//description
		$ticket->content['description'] = "Pork Pi";
		//serial number
		$ticket->content['serialNumber'] = "GSC-".\Input::post("event");
		//date
		$ticket->content['relevantDate'] = "2012-11-08T19:00+00:00";
		//barcode
		$ticket->content['barcode']['message'] = \Input::post("name");
		$ticket->content['barcode']['altText'] = \Input::post("name");


		
		$ticket->write_pass_to_json_file();
		
		$ticket->write_recursive_manifest();
		
		//path, password
		$ticket->generate_bundle_signature(APPPATH,"12345");
		
		$pass_file = $ticket->generate_pass_bundle();

		if (\Input::post("email_pass")) {
				//email
			\Package::load('swiftmailer');
			//Create the Transport
			 $transport = Swift_MailTransport::newInstance();

			 //Create the Mailer using your created Transport
			 $mailer = Swift_Mailer::newInstance($transport);

			 //Create a message
			 $message = Swift_Message::newInstance('Your Ticket')
			   ->setFrom(array('noreply@geeksocialclub.com' => 'The Geek Social Club'))
			   ->setTo(array(\Input::post("email")=>\Input::post("name")))
			   ->setBody('Ticket Attached');

			$attachment = Swift_Attachment::fromPath($pass_file, 'application/vnd.apple.pkpass');
			$message->attach($attachment);
			   
			 //Send the message
			 $mailer->send($message);
			

			$this->template->title = 'Thanks';
			$this->template->content = View::forge('event/thanks');
		}
		else
		{
			File::download($pass_file,null,"application/vnd.apple.pkpass");
		}
		
	}
}
