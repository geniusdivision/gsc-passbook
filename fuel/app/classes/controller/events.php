<?php
class Controller_Events extends Controller_Template 
{

	public function action_index()
	{
		$data['events'] = Model_Event::find('all');
		$this->template->title = "Events";
		$this->template->content = View::forge('events/index', $data);

	}

	public function action_view($id = null)
	{
		$data['event'] = Model_Event::find($id);

		is_null($id) and Response::redirect('Events');

		$this->template->title = "Event";
		$this->template->content = View::forge('events/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Event::validate('create');
			
			if ($val->run())
			{
				$event = Model_Event::forge(array(
					'name' => Input::post('name'),
					'date' => Input::post('date'),
					'location' => Input::post('location'),
				));

				if ($event and $event->save())
				{
					Session::set_flash('success', 'Added event #'.$event->id.'.');

					Response::redirect('events');
				}

				else
				{
					Session::set_flash('error', 'Could not save event.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Events";
		$this->template->content = View::forge('events/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('Events');

		$event = Model_Event::find($id);

		$val = Model_Event::validate('edit');

		if ($val->run())
		{
			$event->name = Input::post('name');
			$event->date = Input::post('date');
			$event->location = Input::post('location');

			if ($event->save())
			{
				Session::set_flash('success', 'Updated event #' . $id);

				Response::redirect('events');
			}

			else
			{
				Session::set_flash('error', 'Could not update event #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$event->name = $val->validated('name');
				$event->date = $val->validated('date');
				$event->location = $val->validated('location');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('event', $event, false);
		}

		$this->template->title = "Events";
		$this->template->content = View::forge('events/edit');

	}

	public function action_delete($id = null)
	{
		if ($event = Model_Event::find($id))
		{
			$event->delete();

			Session::set_flash('success', 'Deleted event #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete event #'.$id);
		}

		Response::redirect('events');

	}


}